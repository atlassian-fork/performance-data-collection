# README #

This is the Atlassian Performance Data Collection toolkit. It is used to collecting data to troubleshoot performance problems.
Currently it is a server-side standalone application that exposes a series of REST APIs for collecting performance data.

### Java Support ###

Currently **only Java 8 JDK is supported**.

### Instructions ###

Please see [How to use the Performance Data Collector](https://confluence.atlassian.com/x/zaAZOQ) for full details on how to install & use it.

### Advanced Settings ###

The following options are configurable:

* Cpu Usage Sampling period. The default is 1s, it can be overriden with `-Dmbean.cpu.sample.sleep=<ms to sample for>` If you increase this over 5s the below will also need to be increased.
* Collection timeout period. The default is 5s, it can be overriden with `-Dcollection.timeout=<s to wait for>`. For example `java -jar -Dcollection.timeout=30 data-collector-1.0.3.jar` to set it to 30s.

### Contribution guidelines ###

* The [Development Guide](https://bitbucket.org/atlassian/performance-data-collection/wiki/Development%20Guide) is the place to start.
* Put in a PR if you want to contribute, the default reviewers will be selected automatically.
* Must follow coding standards such as having unit tests and follow the existing formatting style.
* Ensure squash on commit is used when PR is approved to keep master clean.

### Who do I talk to? ###

* Please raise any issues in the issue tracker for any new features / bugs.
* [David Currie](mailto:dcurrie@atlassian.com) is the primary owner, preferred contact is through [issues](https://bitbucket.org/atlassian/performance-data-collection/issues).
* "CSS Server Eng" is the chat room for Atlassians.