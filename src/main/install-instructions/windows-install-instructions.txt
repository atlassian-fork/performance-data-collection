* The toolkit must be run as the same user that the Atlassian application is run as due to Java JDK limitations.
* This should work by default for all Atlassian products in Windows except Bitbucket Server as the service runs as a specific user.
* It requires Java JDK 8 to be specified in the System %PATH% first, due to a limit in the installer.
* Install it with InstallAsService.bat - this must be run as an Administrator user. It will put it into the standard Windows services along with the other Atlassian applications.
* If you need to change the user (say the Atlassian product is run as a specific user, such as in BitBucket Server), uncomment the below and add the user / password in the windows-service\service.xml and uninstall / reinstall to run it as that user.
    <serviceaccount>
       <domain>YOURDOMAIN</domain>
       <user>useraccount</user>
       <password>Pa55w0rd</password>
       <allowservicelogon>true</allowservicelogon>
    </serviceaccount>
* It can now be accessed on http://localhost:4242, or the hostname of the app if the network is configured to point to it.