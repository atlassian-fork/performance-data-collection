package com.atlassianlabs.perftools.collectors.cpu;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.helper.RunTimeHelper;
import com.atlassianlabs.perftools.services.FileSystemService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Attempts to use the native top command line on *nix distros to print cpu info.
 */
@Component
public class TopCpuUsage implements CpuUsageCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(TopCpuUsage.class);
    private static final int PRIORITY = 0;

    private final FileSystemService fileSystemService;
    private final RunTimeHelper runTimeHelper;
    private final TimeLimitedExecutor timeLimitedExecutor;

    @Autowired
    public TopCpuUsage(@NotNull final FileSystemService fileSystemService,
                       @NotNull final RunTimeHelper runTimeHelper,
                       @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.fileSystemService = requireNonNull(fileSystemService);
        this.runTimeHelper = requireNonNull(runTimeHelper);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    /**
     * Generates CPU information by spawning a bash shell to execute an OS-specific command to query thread info.
     * Supports Linux / Solaris only. Mac doesn't give thread output in top, and
     *
     * @param pid The Java process ID to collect CPU usage info for.
     * @return A String listing the top for the process if can be looked up
     * @throws Exception if unable to convert the bash results into a string, executor service fails or the OS is not supported
     */
    @Override
    public String collectFromProcess(long pid) throws Exception {
        return timeLimitedExecutor.runSync(() -> getCpuResult(pid).orElseThrow(() -> new IOException(String.format("Unable to collect CPU usage results for pid %s.", pid))));
    }

    /**
     * Generates CPU information as per {@link TopCpuUsage#collectFromProcess(long)}.
     *
     * @param pid        The Java process ID to collect CPU usage info for.
     * @param sampleTime Ignored in this implementation as there is no sample time required for top / prstat.
     * @return A String listing the top for the process if can be looked up
     * @throws Exception if unable to convert the bash results into a string, executor service fails or the OS is not supported
     */
    @Override
    public String collectFromPid(long pid, long sampleTime) throws Exception {
        return collectFromProcess(pid);
    }

    // Attempts to spawn a bash shell to execute the appropriate CPU command.
    private Optional<String> getCpuResult(long pid) {
        return runTimeHelper.spawnProcessSafely("bash", "-c", getCpuCommand() + pid)
                .map(process -> {
                    try (final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                        return IOUtils.toString(reader);
                    } catch (IOException e) {
                        LOGGER.warn("Failed to convert reader {}.", getCpuCommand(), e);
                    }
                    return null;
                });
    }

    // Looks up the OS and returns the relevant top command to execute to retrieve per-thread CPU stats.
    private String getCpuCommand() {
        if (fileSystemService.isLinux()) {
            return "/usr/bin/top -b -H -n 1 -p ";
        } else if (fileSystemService.isSolaris()) {
            return "/usr/bin/prstat -L -n 500 1 1 -p ";
        }
        throw new UnsupportedOperationException(String.format("Operating system '%s' is not supported", fileSystemService.getOperatingSystem()));
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
