package com.atlassianlabs.perftools.collectors.diskspeed;

import com.google.common.annotations.VisibleForTesting;
import io.swagger.annotations.ApiModelProperty;

import java.util.function.Function;

/**
 * Container for a disk speed operations such as open, read/write. Contains a callable that defines the operation
 * and a {@link DiskSpeedTimerList} that is used for measuring the speed of the operation.
 */
public class DiskSpeedOperation<I, O> {
    private final Function<I, O> function;

    @ApiModelProperty(value = "The name of the disk speed operation", example = "open", required = true)
    private final String name;

    private DiskSpeedTimerList diskSpeedTimerList = new DiskSpeedTimerList();

    public DiskSpeedOperation(final String name, Function<I, O> function) {
        this.name = name;
        this.function = function;
    }

    @VisibleForTesting
    public void setDiskSpeedTimerList(DiskSpeedTimerList diskSpeedTimerList) {
        this.diskSpeedTimerList = diskSpeedTimerList;
    }

    public String getName() {
        return name;
    }

    @ApiModelProperty(value = "The results of the disk speed operation", required = true)
    public DiskSpeedTimerList getResults() {
        return diskSpeedTimerList;
    }

    O run(I i) {
        final DiskSpeedTimer timer = diskSpeedTimerList.newTimer();
        try {
            return function.apply(i);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            timer.stop();
        }
    }

    @Override
    public String toString() {
        return diskSpeedTimerList.toString();
    }
}
