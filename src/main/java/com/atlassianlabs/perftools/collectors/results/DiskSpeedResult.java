package com.atlassianlabs.perftools.collectors.results;

import com.atlassianlabs.perftools.collectors.diskspeed.DiskSpeedOperation;
import com.atlassianlabs.perftools.collectors.diskspeed.DiskSpeedTimerList;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthLongestLine;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import io.swagger.annotations.ApiModelProperty;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.ImmutableList.copyOf;
import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toMap;

/**
 * Contains results of the disk speed benchmarking.
 */
public class DiskSpeedResult {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @ApiModelProperty(value = "The directory used to generate the Java disk speed results",
            example = "/var/atlassian/application-data/jira", required = true)
    private String directory;

    @ApiModelProperty(value = "A list of operations that were executed to test the disk speed", required = true)
    private List<DiskSpeedOperation> operations;

    public DiskSpeedResult(final String directory, final List<DiskSpeedOperation> operations) {
        this.directory = directory;
        this.operations = copyOf(operations);
    }

    public String getDirectory() {
        return directory;
    }

    public List<DiskSpeedOperation> getOperations() {
        return operations; //NOSONAR
    }

    /**
     * This exists for analytics, so we can send the averages of disk speed operations.
     *
     * @return A map of the name of the operation and its average speed in nanos.
     */
    @JsonIgnore
    public Map<String, String> getAveragesOfOperations() {
        return getOperations().stream().collect(toMap(
                operation -> format("disk.speed.operation.%s.average", operation.getName()),
                operation -> operation.getResults().getAverage()));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append(now().format(DATE_FORMAT));
        sb.append("\n");
        sb.append("Directory tested: ").append(directory);
        sb.append("\n");

        V2_AsciiTable table = new V2_AsciiTable();
        table.addStrongRule();
        table.addRow(null, null, null, null, "TOTALS");
        table.addStrongRule();
        table.addRow("stat", "avg", "median", "tmin", "tmax");

        for (final DiskSpeedOperation operation : getOperations()) {
            final DiskSpeedTimerList timer = operation.getResults();
            table.addRule();
            table.addRow(operation.getName(), timer.getAverage(), timer.getMedian(), timer.getMin(), timer.getMax());
        }
        table.addStrongRule();

        final V2_AsciiTableRenderer renderer = new V2_AsciiTableRenderer();
        renderer.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
        renderer.setWidth(new WidthLongestLine());

        sb.append(renderer.render(table));
        sb.append("All times are in nanoseconds.");

        return sb.toString();
    }
}
