package com.atlassianlabs.perftools.collectors.results;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * CumulativeResult is extended with nothing overridden purely so we can give specific REST API docs for these results.
 */
public class ThreadDumpResult extends CumulativeResult {

    public ThreadDumpResult(List<String> failureReasons, String generatedBy, String result) {
        super(failureReasons, generatedBy, result);
    }

    @ApiModelProperty(value = "The name of the thread dump collector that generated these results. Uses Spring bean name",
            example = "JStackThreadDump", required = true)
    public String getGeneratedBy() {
        return super.getGeneratedBy();
    }

    // No example because swagger doesn't properly handle example Lists currently, so the spec looks wrong.
    @ApiModelProperty(value = "A list of any failure reasons that may have been encountered when attempting to collect results", required = true)
    public List<String> getFailureReasons() {
        return super.getFailureReasons();
    }

    @ApiModelProperty(value = "A string containing the result of the successful collection. Will be empty if all collectors failed",
            example = "2017-06-14 13:32:04\nFull thread dump \n\n\"JMX server connection timeout 200\" prio=9 ...")
    public String getResult() {
        return super.getResult();
    }
}
