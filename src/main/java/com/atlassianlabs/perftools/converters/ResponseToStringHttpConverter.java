package com.atlassianlabs.perftools.converters;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.TEXT_PLAIN;

/**
 * Because Spring is being a total jerk and isn't using the FallbackObjectToStringConverter for our responses.
 */
public class ResponseToStringHttpConverter extends AbstractHttpMessageConverter {
    @Override
    protected boolean supports(Class clazz) {
        return true;
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return singletonList(TEXT_PLAIN);
    }

    @Override
    protected void writeInternal(Object o, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        final Charset charset = getContentTypeCharset(outputMessage.getHeaders().getContentType());
        StreamUtils.copy(o.toString(), charset, outputMessage.getBody());
    }

    @Override
    protected Object readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        final Charset charset = getContentTypeCharset(inputMessage.getHeaders().getContentType());
        return StreamUtils.copyToString(inputMessage.getBody(), charset);
    }

    private Charset getContentTypeCharset(MediaType contentType) {
        if (contentType != null && contentType.getCharset() != null) {
            return contentType.getCharset();
        } else {
            return getDefaultCharset();
        }
    }

    @Override
    public Charset getDefaultCharset() {
        return Charset.defaultCharset();
    }
}
