package com.atlassianlabs.perftools.product;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.services.FileSystemService;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static java.util.Objects.requireNonNull;

/**
 * Factory for getting access to a {@link DescriptorConverter}.
 */
@Component
public class DescriptorConverterFactory {
    private final FileSystemService fileSystemService;
    private final TimeLimitedExecutor timeLimitedExecutor;
    

    @Autowired
    public DescriptorConverterFactory(@NotNull FileSystemService fileSystemService, @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.fileSystemService = requireNonNull(fileSystemService);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    /**
     * Constructs a {@link DescriptorConverter} with the provided {@link JavaProcessDetails}.
     *
     * @param processDetails The relevant process details that will be attempted to be converted into a product.
     * @return A new {@link DescriptorConverter} object containing any required dependencies and the process details.
     */
    public DescriptorConverter getDescriptor(JavaProcessDetails processDetails) {
        return new DescriptorConverter(fileSystemService, timeLimitedExecutor, processDetails);
    }
}
