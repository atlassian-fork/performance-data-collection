package com.atlassianlabs.perftools.product;

import io.swagger.annotations.ApiModelProperty;

public class Product {
    @ApiModelProperty(value = "The process id of the Atlassian product", example = "1405", required = true)
    private final long pid;
    @ApiModelProperty(value = "The path to the home directory of the Atlassian product",
            example = "/var/atlassian/application-data/jira", required = true)
    private final String homeDir;
    @ApiModelProperty(value = "The path to the installation directory of the Atlassian product",
            example = "/opt/atlassian/jira", required = true)
    private final String installDir;
    @ApiModelProperty(value = "The name of the Atlassian product", example = "JIRA", required = true)
    private final String name;
    @ApiModelProperty(value = "The path to the default log directory of the Atlassian product",
            example = "/opt/atlassian/jira/log", required = true)
    private final String logFileDir;

    /**
     * Product object used for data collection.
     *
     * @param pid        The Process id of the product.
     * @param name       Location of the product name.
     * @param homeDir    Location of the product homeDir path.
     * @param installDir Location of the product installDir path.
     * @param logFileDir Location of the log directory.
     */
    public Product(final long pid, final String name, final String homeDir, final String installDir, final String logFileDir) {
        this.pid = pid;
        this.name = name;
        this.homeDir = homeDir;
        this.installDir = installDir;
        this.logFileDir = logFileDir;
    }

    public long getPid() {
        return pid;
    }

    public String getName() {
        return name;
    }

    public String getHomeDir() {
        return homeDir;
    }

    public String getInstallDir() {
        return installDir;
    }

    public String getLogFileDir() {
        return logFileDir;
    }

    @Override
    public String toString() {
        return getName();
    }
}
