package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FisheyeDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        // Oh FishEye you special snowflake! Has a special displayName, e.g.: "/Users/triforce/Downloads/fecru-4.2.1/fisheyeboot.jar start"
        return "fisheyeboot.jar start";
    }

    @Override
    public String getInstProp() {
        return "fisheye.inst";
    }

    @Override
    public String getHomeProp() {
        return getInstProp();
    }

    @Override
    public String getLogPath() {
        return "var" + File.separator + "log";
    }

    @Override
    public String getName() {
        return "FishEye";
    }
}
