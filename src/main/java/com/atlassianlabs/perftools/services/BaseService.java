package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.collectors.Collector;
import com.atlassianlabs.perftools.collectors.results.CumulativeResult;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparingInt;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCause;

/**
 * This abstract class exists to allow for different implementations of collectors to use the same collection code.
 *
 * @param <C> The collector implementation to use, e.g.: CpuUsageCollector.
 * @param <R> The collector result to use, e.g.: CpuUsageResult
 */
abstract class BaseService<C extends Collector, R extends CumulativeResult> {
    private final AnalyticsService analyticsService;
    private final ApplicationContext context;
    private final Class<C> collectorClass;
    private final Logger logger;

    BaseService(@NotNull final AnalyticsService analyticsService,
                @NotNull final ApplicationContext context,
                @NotNull final Class<C> collectorClass) {
        this.analyticsService = requireNonNull(analyticsService);
        this.context = requireNonNull(context);
        this.collectorClass = requireNonNull(collectorClass);
        this.logger = LoggerFactory.getLogger(collectorClass);
    }

    /**
     * This accesses the {@link ApplicationContext} to get any beans that implement the {@link BaseService#collectorClass}.
     * It then sorts them by priority and returns them.
     *
     * @return A Map of classes that are ordered by priority, with the Spring bean name as the key.
     */
    public Map<String, C> getCollectors() {
        return context.getBeansOfType(collectorClass).entrySet().stream()
                .sorted(comparingInt(e -> e.getValue().getPriority()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (u, v) -> {
                    throw new IllegalStateException(format("Duplicate key %s", u));
                }, LinkedHashMap::new));
    }

    /**
     * Iterates through all of the implemented collectors as defined in {@link BaseService#collectorClass}. It then
     * attempts to collect data using each one until it is successful. This will catch any exceptions thrown by each
     * collector and fail over to the next one, if it is present, until all have been tried.
     *
     * @param pid The Java process ID to attempt to collect data for.
     * @return A result object containing the result and any failure reasons that may have occurred with the different collectors.
     */
    public R collectFromPid(final long pid) {
        return getCumulativeResultFromCollectors(pid, getCollectors().values());
    }

    /**
     * Allows for collecting from the PID with a specific collector specified.
     *
     * @param pid           The Java process ID to attempt to collect data for.
     * @param collectorName The name of the collector, mapped to the Spring bean name returned from  {@link BaseService#getCollectors()}.
     * @return A result object containing the result and any failure reasons that may have occurred with the different collectors.
     * @throws NotFoundException If the provided collectorName is not a valid collector (e.g.: doesn't exist).
     */
    public R collectFromPid(final long pid, final String collectorName) throws NotFoundException {
        final C collector = getCollectors().get(collectorName);
        if (collector == null) {
            throw new NotFoundException(format("Unable to locate collector %s.", collectorName));
        }
        return getCumulativeResultFromCollectors(pid, singletonList(collector));
    }

    private R getCumulativeResultFromCollectors(final long pid, Collection<C> collectors) {
        // Iterate through all the provided collectors, attempting to retrieve a result and keeping track of any failed attempts.
        final List<String> failureReasons = new ArrayList<>();
        final List<String> failedCollectors = new ArrayList<>();
        for (final C collector : collectors) {
            try {
                logger.debug("Attempting to collect with {}.", collector.getName());
                final R result = newResult(failureReasons, collector.getName(), collector.collectFromProcess(pid));
                sendAnalytics(collector.getName(), failedCollectors);
                return result;
            } catch (Exception e) { //NOSONAR
                failureReasons.add(format("Unable to collect with %s: %s", collector.getName(), ofNullable(getRootCause(e)).orElse(e).getLocalizedMessage()));
                failedCollectors.add(collector.getName());
                logger.info("Unable to collect with {} due to an exception.", collector.getName(), e);
            }
        }
        // Completely failed result will only have failure reasons:
        sendAnalytics("N/A", failedCollectors);
        return newResult(failureReasons, "", "");
    }

    /**
     * Takes the details of the collector and its attempts, then sends it to the analytics.
     *
     * @param collectorName    The name of the collector that was used.
     * @param failedCollectors A list of collectors that failed.
     */
    private void sendAnalytics(String collectorName, List<String> failedCollectors) {
        analyticsService.sendAsyncEvent(
                format("pdc.server.collect.%s", collectorClass.getSimpleName()),
                asMap("collector.name", collectorName, "failed.collectors", join(",", failedCollectors))
        );
    }

    /**
     * Constructs the appropriate result object as defined in the implementation.
     *
     * @param failureReasons A list of failure reasons for any of the collectors
     * @param generatedBy    The collector that succesfully generated the result
     * @param result         The result of the collection
     * @return A The result of the collection, successful or not. Type is determined by the implementation
     */
    protected abstract R newResult(List<String> failureReasons, String generatedBy, String result);
}
