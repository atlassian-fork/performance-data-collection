package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.collectors.results.ComboResult;
import com.atlassianlabs.perftools.collectors.results.CpuUsageResult;
import com.atlassianlabs.perftools.collectors.results.ThreadDumpResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Provides a way of collecting multiple data at once, delegating to the other services to retrieve it.
 */
@Component
public class ComboService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComboService.class);

    private final TimeService timeService;
    private final CpuUsageService cpuUsageService;
    private final ThreadDumpService threadDumpService;

    @Autowired
    public ComboService(@NotNull final TimeService timeService, @NotNull final CpuUsageService cpuUsageService,
                        @NotNull final ThreadDumpService threadDumpService) {
        this.timeService = requireNonNull(timeService);
        this.cpuUsageService = requireNonNull(cpuUsageService);
        this.threadDumpService = requireNonNull(threadDumpService);
    }

    /**
     * Generates CPU and thread dump info for a specific process. Allows for specifyig a total number of times and the wait
     * time between collections. This is a blocking thread approach and is used for ease of writing it. When the task
     * is complete it returns a result that contains all of the collected data.
     *
     * @param pid        Process id to collect data for
     * @param totalTimes Total times to execute thread dump + cpu collection
     * @param waitTime   How long to wait between executions
     * @throws InterruptedException if the thread is interrupted whilst sleeping
     */
    public ComboResult collectThreadDumpsAndCpuUsage(final long pid, final int totalTimes, final long waitTime) throws InterruptedException {
        LOGGER.info("Collecting {} dumps, over approximately {} minute(s). ", totalTimes, MILLISECONDS.toMinutes(totalTimes * waitTime));

        List<ThreadDumpResult> threadDumpResults = new ArrayList<>();
        List<CpuUsageResult> cpuUsageResults = new ArrayList<>();
        for (int i = 1; i <= totalTimes; i++) {
            LOGGER.info("Generating {} of {} dumps.", i, totalTimes);
            cpuUsageResults.add(cpuUsageService.collectFromPid(pid));
            threadDumpResults.add(threadDumpService.collectFromPid(pid));

            if (i != totalTimes) {
                LOGGER.info("Waiting {} seconds before collecting another data dump.", MILLISECONDS.toSeconds(waitTime));
                timeService.sleep(waitTime);
            }
        }
        return new ComboResult(cpuUsageResults, threadDumpResults);
    }
}
