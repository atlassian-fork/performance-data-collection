package com.atlassianlabs.perftools.services;

import org.gridkit.lab.jvm.attach.AttachManager;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.gridkit.lab.jvm.attach.JavaProcessId;
import org.springframework.stereotype.Service;

import javax.management.MBeanServerConnection;
import javax.validation.constraints.NotNull;
import java.net.ConnectException;
import java.util.List;

/**
 * This service exists to wrap the {@link AttachManager} so we can mock the results for testing, and also provide
 * some validation on the results returned. Additionally it provides some virtual-machine services, such as checking
 * if a process is a Java process.
 */
@Service
public class VirtualMachineService {
    /**
     * "Proxies" to {@link AttachManager#getDetails(long)}.
     *
     * @param pid A long of the process id.
     * @return The Java process details of the attached process id.
     */
    @NotNull
    public JavaProcessDetails getProcessDetails(long pid) {
        return AttachManager.getDetails(pid);
    }

    /**
     * "Proxies" to {@link AttachManager#listJavaProcesses()}.
     *
     * @return A list of all the running Java processes.
     */
    public List<JavaProcessId> listRunningVirtualMachines() {
        return AttachManager.listJavaProcesses();
    }

    /**
     * "Proxies" to {@link JavaProcessDetails#getMBeans()} and verifies a server connection is returned.
     *
     * @param pid A long of the process id.
     * @return The mbean server connection for the process id.
     * @throws ConnectException if the Java process cannot be connected to with the {@link MBeanServerConnection}.
     */
    public MBeanServerConnection getMBeanServerConnection(long pid) throws ConnectException {
        final MBeanServerConnection serverConnection = getProcessDetails(pid).getMBeans();
        if (serverConnection == null) {
            throw new ConnectException(String.format("A MBean server connection was not found or able to be accessed for pid %s.", pid));
        }
        return serverConnection;
    }
}
