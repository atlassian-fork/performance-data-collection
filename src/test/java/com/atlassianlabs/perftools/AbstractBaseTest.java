package com.atlassianlabs.perftools;

import com.atlassianlabs.perftools.collectors.results.CpuUsageResult;
import com.atlassianlabs.perftools.collectors.results.ThreadDumpResult;
import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.product.Product;
import org.junit.Before;
import org.mockito.Mock;

import java.io.File;
import java.util.concurrent.Callable;

import static com.atlassianlabs.perftools.JsonConverter.convertObjectToJson;
import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Has a bunch of diff vars and objects for testing. Can also put in shared methods if need be.
 */
public abstract class AbstractBaseTest {
    // Multi-line strings so we make sure the whole thing is input into the results:
    protected static final String CPU_RESULT_STRING = "Where my droids at\n" +
            "they all up in here?\n" +
            "cause like i dunno man";
    protected static final String THREAD_DUMP_RESULT_STRING = "for a star to be born\n" +
            "there is one thing that must happen:\n" +
            "a gaseous nebulae must collapse.";

    protected static final String CONF_HOME_DIR = "/var/atlassian/application-data/confluence";
    protected static final String JIRA_HOME_DIR = "C:\\Program Files\\Atlassian\\Application Data\\JIRA7.2.1";
    protected static final String JIRA_INSTALL_DIR = "/opt/atlassian/jira/";
    protected static final String CONF_INSTALL_DIR = "/users/potato/installations/confluence";
    protected static final String FISH_INSTALL_DIR = "/Users/captainplanet/Applications/fecru-4.2.1";
    protected static final String BITBUCKET_INSTALL_DIR = "/Users/lancelot/Applications/Bitbucket/4.11.0";
    protected static final String LOG_DIR = "/tmp/log/potato";
    protected static final long JIRA_PID = 8098L;
    protected static final long CONF_PID = 29600L;
    protected static final long FISH_PID = 1960L;
    protected static final long UNKNOWN_PID = 4242L;

    protected File jiraHomeDirFile;
    protected File confHomeDirFile;

    protected Product jiraProduct;
    protected Product confProduct;
    protected Product fishEyeProduct;

    protected String jiraProductJson;
    protected String confProductJson;
    protected String fishEyeProductJson;

    protected CpuUsageResult successfulCpuUsageResult;
    protected ThreadDumpResult successfulThreadDumpResult;

    @Mock
    protected TimeLimitedExecutor timeLimitedExecutor;

    @Before
    public void setUp() throws Exception {
        jiraHomeDirFile = new File(JIRA_INSTALL_DIR, "atlassian-jira/WEB-INF/classes/jira-application.properties");
        confHomeDirFile = new File(CONF_INSTALL_DIR, "confluence/WEB-INF/classes/confluence-init.properties");

        jiraProduct = new Product(JIRA_PID, "JIRA", JIRA_HOME_DIR, JIRA_INSTALL_DIR, LOG_DIR);
        confProduct = new Product(CONF_PID, "Confluence", CONF_HOME_DIR, CONF_INSTALL_DIR, LOG_DIR);
        fishEyeProduct = new Product(FISH_PID, "FishEye", FISH_INSTALL_DIR, FISH_INSTALL_DIR, LOG_DIR);

        jiraProductJson = convertObjectToJson(jiraProduct);
        confProductJson = convertObjectToJson(confProduct);
        fishEyeProductJson = convertObjectToJson(fishEyeProduct);

        successfulCpuUsageResult = new CpuUsageResult(emptyList(), "MBeanCpuUsage", CPU_RESULT_STRING);
        successfulThreadDumpResult = new ThreadDumpResult(emptyList(), "JStackThreadDump", THREAD_DUMP_RESULT_STRING);

        when(timeLimitedExecutor.runSync(any(Callable.class))).thenAnswer(invocation -> {
            final Object[] args = invocation.getArguments();
            final Callable callable = (Callable) args[0];
            return callable.call();
        });
    }
}
