package com.atlassianlabs.perftools.collectors.diskspeed;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Arrays.stream;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DiskSpeedTimerListTest {
    private static final String EXPECTED_AVERAGE = "350,000";
    private static final String EXPECTED_MAX = "450,000";
    private static final String EXPECTED_MIN = "270,000";
    private static final String EXPECTED_MEDIAN = "380,000";

    private DiskSpeedTimerList diskSpeedTimerList;

    @Before
    public void setUp() throws Exception {
        diskSpeedTimerList = new DiskSpeedTimerList();

        stream(new long[]{300000L, 270000L, 380000L, 450000L}).forEach(total -> {
            final DiskSpeedTimer timer = new DiskSpeedTimer();
            timer.setTotal(total);
            diskSpeedTimerList.addTimer(timer);
        });
    }

    @Test
    public void averageShouldReturnExpected() throws Exception {
        assertThat(diskSpeedTimerList.getAverage(), is(EXPECTED_AVERAGE));
    }

    @Test
    public void medianShouldReturnExpected() throws Exception {
        assertThat(diskSpeedTimerList.getMedian(), is(EXPECTED_MEDIAN));
    }

    @Test
    public void maxShouldReturnExpected() throws Exception {
        assertThat(diskSpeedTimerList.getMax(), is(EXPECTED_MAX));
    }

    @Test
    public void minShouldReturnExpected() throws Exception {
        assertThat(diskSpeedTimerList.getMin(), is(EXPECTED_MIN));
    }

    @Test
    public void compareToAndEqualsShouldWorkAsExpected() throws Exception {
        final DiskSpeedTimer timer1 = new DiskSpeedTimer();
        timer1.setTotal(10);
        final DiskSpeedTimer timer2 = new DiskSpeedTimer();
        timer2.setTotal(10);

        assertThat(timer1.compareTo(timer2), equalTo(0));
        assertThat(timer1.equals(timer2), equalTo(true));
        assertThat((timer1.compareTo(timer2)==0) == (timer1.equals(timer2)), is(true));
    }
}