package com.atlassianlabs.perftools.product;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.product.descriptors.ConfluenceDescriptor;
import com.atlassianlabs.perftools.product.descriptors.FisheyeDescriptor;
import com.atlassianlabs.perftools.product.descriptors.JiraDescriptor;
import com.atlassianlabs.perftools.product.descriptors.ProductDescriptor;
import com.atlassianlabs.perftools.services.FileSystemService;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

import static java.util.Optional.empty;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DescriptorConverterTest extends AbstractBaseTest {
    @Mock
    private JavaProcessDetails processDetails;
    @Mock
    private FileSystemService fileSystemService;

    private Properties systemProperties;
    private Properties homeDirFileProperties;

    private ProductDescriptor fisheyeDescriptor = new FisheyeDescriptor();
    private ProductDescriptor jiraDescriptor = new JiraDescriptor();
    private ProductDescriptor confDescriptor = new ConfluenceDescriptor();

    @InjectMocks
    private DescriptorConverter descriptorConverter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        systemProperties = new Properties();
        homeDirFileProperties = new Properties();

        when(processDetails.getSystemProperties()).thenReturn(systemProperties);
        when(fileSystemService.isUnix()).thenReturn(true);
    }

    @Test
    public void convertFailDueToDescriptionNotMatch() {
        when(processDetails.getDescription()).thenReturn(fisheyeDescriptor.getDisplayName());

        assertEmptyResults(jiraDescriptor);
    }

    @Test
    public void convertFailDueToInstallDirNotMatch() {
        when(processDetails.getDescription()).thenReturn(confDescriptor.getDisplayName());
        systemProperties.put(jiraDescriptor.getInstProp(), CONF_INSTALL_DIR);

        assertEmptyResults(jiraDescriptor);
    }

    @Test
    public void convertFailDueToInstallDirNull() {
        when(processDetails.getDescription()).thenReturn(confDescriptor.getDisplayName());

        assertEmptyResults(jiraDescriptor);
    }

    @Test
    public void convertFailDueToHomeDirFileNotValid() {
        when(processDetails.getDescription()).thenReturn(jiraDescriptor.getDisplayName());
        systemProperties.put(jiraDescriptor.getInstProp(), JIRA_INSTALL_DIR);
        when(fileSystemService.isValidFile(any(File.class))).thenReturn(false);

        assertEmptyResults(jiraDescriptor);
    }

    @Test
    public void convertReturnsJiraWithoutParsingHomePropFile() {
        when(processDetails.getDescription()).thenReturn(jiraDescriptor.getDisplayName());
        when(processDetails.getPid()).thenReturn(JIRA_PID);
        systemProperties.put(jiraDescriptor.getInstProp(), JIRA_INSTALL_DIR);
        systemProperties.put(jiraDescriptor.getHomeProp(), JIRA_HOME_DIR);

        assertProductResults(jiraDescriptor, jiraProduct);
    }

    @Test
    public void convertReturnsConfluenceByParsingHomePropFile() throws Exception {
        when(processDetails.getDescription()).thenReturn(confDescriptor.getDisplayName());
        when(processDetails.getPid()).thenReturn(CONF_PID);
        systemProperties.put(confDescriptor.getInstProp(), CONF_INSTALL_DIR);
        homeDirFileProperties.put(confDescriptor.getHomeProp(), CONF_HOME_DIR);
        when(fileSystemService.isValidFile(confHomeDirFile)).thenReturn(true);
        when(fileSystemService.parsePropertiesFromFile(confHomeDirFile)).thenReturn(homeDirFileProperties);

        assertProductResults(confDescriptor, confProduct);
    }

    @Test
    public void convertReturnsFishEyeSkipsHomeDirLogic() {
        when(processDetails.getDescription()).thenReturn("/Users/triforce/Downloads/fecru-4.2.1/fisheyeboot.jar start");
        when(processDetails.getPid()).thenReturn(FISH_PID);
        systemProperties.put(fisheyeDescriptor.getInstProp(), FISH_INSTALL_DIR);

        assertProductResults(fisheyeDescriptor, fishEyeProduct);
        verify(fileSystemService, times(0)).isValidFile(any());
    }

    @Test
    public void convertIsEmptyBecauseThrowsExceptionWhenParsingHomePropFile() throws Exception {
        when(processDetails.getDescription()).thenReturn(jiraDescriptor.getDisplayName());
        systemProperties.put(jiraDescriptor.getInstProp(), JIRA_INSTALL_DIR);
        when(fileSystemService.isValidFile(jiraHomeDirFile)).thenReturn(true);
        when(fileSystemService.parsePropertiesFromFile(jiraHomeDirFile)).thenThrow(new IOException("AWW SHIT SON"));

        assertEmptyResults(jiraDescriptor);
    }

    @Test
    public void itShouldReturnEmptyOptionalIfExecutorThrowsException() throws Exception {
        doThrow(new UncheckedTimeoutException()).when(timeLimitedExecutor).runSync(any());

        assertEmptyResults(jiraDescriptor);
    }

    private void assertEmptyResults(ProductDescriptor descriptor) {
        final Optional<Product> results = descriptorConverter.apply(descriptor);
        assertThat(results, is(empty()));
    }

    private void assertProductResults(ProductDescriptor descriptor, Product product) {
        final Optional<Product> results = descriptorConverter.apply(descriptor);

        if (results.isPresent()) {
            assertThat(results.get().getPid(), is(product.getPid()));
            assertThat(results.get().getName(), is(product.getName()));
            assertThat(results.get().getInstallDir(), is(product.getInstallDir()));
            assertThat(results.get().getHomeDir(), is(product.getHomeDir()));
        } else {
            fail("No product is present!");
        }
    }
}
